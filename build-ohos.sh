set -e
rm -rf ./out ./minilisp
docker run --rm \
  -e HARDWARE=ipcamera_hi3516dv300 \
  -v ${PWD}/out:/OpenHarmony/out \
  -v ${PWD}:/OpenHarmony/applications/sample/camera/app \
  ystyle/open-harmony 
cp ./out/ipcamera_hi3516dv300/bin/camera_app minilisp

